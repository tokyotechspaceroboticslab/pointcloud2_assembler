/*
 * pointcloud2_assembler.cpp
 *
 *  Created on: Feb 24, 2015
 *      Author: Akihiko Honda
 *      Based on dimitri prosser's code : http://pastebin.com/gsmCGweU
 */

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/conversions.h>
#include <pcl/point_types.h>
#include <tf/transform_listener.h>
#include <Eigen/Core>
#include <boost/algorithm/string.hpp>
#include <std_srvs/Empty.h>
#include <tf/transform_listener.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>

using namespace pcl;

namespace cloud_assembler
{
  
  class CloudAssembler
  {
    
  public:
    CloudAssembler();
    void cloudCallback(const PCLPointCloud2&  cloud);
    
  private:
    ros::NodeHandle node_;
    
    ros::ServiceServer pause_srv_;
    
    ros::Publisher output_pub_;
    ros::Subscriber cloud_sub_;
    
    tf::TransformListener tf_listener;
    
    PointCloud<PointXYZ> assembled_cloud_;
    int buffer_length_;
    std::vector<PointCloud<PointXYZ> > cloud_buffer_;
    bool assemblerPaused_;
    
    //void addToBuffer(PCLPointCloud2 cloud);
    void addToBuffer(PCLPointCloud2 cloud);
    void assembleCloud();
    bool pauseSrv(std_srvs::Empty::Request& req, std_srvs::Empty::Response& resp);

    std::string fixed_frame_name;
    //std::string sensor_frame_name;
    int skip_num, skip_count;

  };
  
  CloudAssembler::CloudAssembler()
  {
    ros::NodeHandle private_nh("~");
    
    private_nh.param("buffer_length", buffer_length_, 50);
    
    output_pub_ = node_.advertise<sensor_msgs::PointCloud2> ("/assembled_cloud", 100);
    
    pause_srv_ = node_.advertiseService("/pause_assembler", &CloudAssembler::pauseSrv, this);
    
    cloud_sub_ = node_.subscribe("/input_cloud", 100, &CloudAssembler::cloudCallback, this);
    
    PointCloud<PointXYZ> clear;
    assembled_cloud_ = clear;
    assemblerPaused_ = false;

    fixed_frame_name="map"; // TODO
    //sensor_frame_name="camera_depth_optical_frame"; // TODO
    skip_num = 10;//TODO
    skip_count = 0;

    ROS_INFO("[PC2Asm] Start");
  }
  
  void CloudAssembler::cloudCallback(const PCLPointCloud2&  cloud)
  {
    if (skip_count >= skip_num){
      addToBuffer(cloud);
      assembleCloud();
    
      PCLPointCloud2 cloud_msg;
      pcl::toPCLPointCloud2(assembled_cloud_, cloud_msg); // PCL->ROS
    
      //cloud_msg.header.frame_id = cloud.header.frame_id;
      cloud_msg.header.frame_id = fixed_frame_name;
        
      output_pub_.publish(cloud_msg);
      skip_count = 0;
    }else{
      skip_count++;
    }
      

  }
  
  void CloudAssembler::assembleCloud()
  {
    ROS_DEBUG("Assembling.");
    
    unsigned int i;
    
    if (assemblerPaused_)
      {
	ROS_INFO("assemblerPaused_ is true");
      }
    if (!assemblerPaused_)
      {
	ROS_DEBUG("assemblerPaused_ is false");
      }
    
    std::string fixed_frame = cloud_buffer_[0].header.frame_id;
    
    PointCloud<PointXYZ> new_cloud;
    new_cloud.header.frame_id = fixed_frame;
    
    int buf_num = cloud_buffer_.size();
    ROS_DEBUG("Current buffer length is %d", buf_num); // TODO
    
    for (i = 0; i < cloud_buffer_.size(); i++)
      {
	PointCloud<PointXYZ> temp_cloud;
	//fromPCLPointCloud2(cloud_buffer_[i], temp_cloud);
	temp_cloud = cloud_buffer_[i];
	temp_cloud.header.frame_id = fixed_frame;
	new_cloud += temp_cloud;
      }
    
    // If it's paused, don't overwrite the stored cloud with a new one, just keep publishing the same cloud
    if (!assemblerPaused_)
      {
	assembled_cloud_ = new_cloud;
      }
    else if (assemblerPaused_)
      {
	ROS_DEBUG("The Assembler will continue to publish the same cloud.");
      }
    
  }
  
  bool CloudAssembler::pauseSrv(std_srvs::Empty::Request& req, std_srvs::Empty::Response& resp)
  {
    ROS_DEBUG("In service call: %s", assemblerPaused_?"true":"false");
    
    if (!assemblerPaused_)
      {
	ROS_DEBUG("Now paused.");
	assemblerPaused_ = true;
      }
    else if (assemblerPaused_)
      {
	assemblerPaused_ = false;
	ROS_DEBUG("Unpaused.");
      }
    
    return true;
  }
  
  void CloudAssembler::addToBuffer(PCLPointCloud2 cloud)
  {
    // Transform pointclouds
    //PCLPointCloud2 cloud_trans;
    PointCloud<PointXYZ> cloud_trans, cloud_pcl;
    fromPCLPointCloud2(cloud, cloud_pcl); // ROS -> PCL
    try{
      //tf_listener.waitForTransform(fixed_frame_name, cloud.header.frame_id, ros::Time::now(), ros::Duration(1.0)); // TODO time
      pcl_ros::transformPointCloud(fixed_frame_name, cloud_pcl, cloud_trans, tf_listener);
    }
    catch (tf::TransformException &ex) {
      ROS_ERROR("[PC2Asm] %s",ex.what());
      ros::Duration(1.0).sleep();
      //TODO;
    }

    int buf_num = cloud_buffer_.size();
    int pts = cloud_trans.width;
    ROS_DEBUG("Adding cloud to buffer. Current buffer length is %d", buf_num);
    ROS_DEBUG("Newest cloud number is %d", pts);
    
    if (cloud_buffer_.size() >= (unsigned int)buffer_length_)
      {
	cloud_buffer_.erase(cloud_buffer_.begin());
      }
    
    cloud_buffer_.push_back(cloud_trans);
  }
  
  
}; // namespace

int main(int argc, char ** argv)
{
  ros::init(argc, argv, "cloud_assembler");
  cloud_assembler::CloudAssembler cloud_assembler;
  
  ros::spin();
  
  return 0;
}
